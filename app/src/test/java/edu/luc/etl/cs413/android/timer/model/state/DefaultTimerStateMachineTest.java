package edu.luc.etl.cs413.android.timer.model.state;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.luc.etl.cs413.android.timer.common.TimerUIUpdateListener;
import edu.luc.etl.cs413.android.timer.model.clock.DefaultClockModel;
import edu.luc.etl.cs413.android.timer.model.time.DefaultTimeModel;

import static org.junit.Assert.assertEquals;

/**
 * Created by juliacicale1 on 12/7/16.
 */

class StateMachineUITestThing implements TimerUIUpdateListener {
    public int timeValue = 0;
    public int stateId = 0;
    public boolean alarming = false;

    @Override
    public void updateState(int stateId) {
        this.stateId = stateId;
    }

    @Override
    public void updateTime(int timeValue) {
        this.timeValue = timeValue;
    }

    @Override
    public void playDefaultAlarm() {
        alarming = true;
    }

    @Override
    public void stopDefaultAlarm() {
        alarming = false;
    }

    @Override
    public void playBeep() {

    }
}

public class DefaultTimerStateMachineTest {

    private DefaultTimerStateMachine stateMachine;
    private StateMachineUITestThing stateMachineUITestThing;

    //set up models and state machine

    @Before
    public void setUp() throws Exception {
        DefaultTimeModel timeModel = new DefaultTimeModel();
        DefaultClockModel clockModel = new DefaultClockModel();
        stateMachine = new DefaultTimerStateMachine(timeModel, clockModel);
        clockModel.setOnTickListener(stateMachine);
        stateMachine.actionInit();
        stateMachineUITestThing = new StateMachineUITestThing();
        stateMachine.setUIUpdateListener(stateMachineUITestThing);
    }

    @After
    public void tearDown() throws Exception {
        stateMachine = null;
    }

    //verifies that state machine is initially in stopped state
    @Test
    public void testInitiallyInStoppedState() {

        StoppedState stoppedState = new StoppedState(stateMachine);
        assertEquals(stateMachine.getState().getId(), stoppedState.getId());

    }

    //tests that when button is pressed, we increment by 1
    @Test
    public void testIncrementRuntimeOne() {
        stateMachine.onStartStop();
        assertEquals(stateMachineUITestThing.timeValue, 1);

    }

    //tests that the counter can go up to a maximum of 99
    @Test
    public void testActivityScenarioIncUntilFull() {
        for (int i = 0; i <101; i ++) {
            stateMachine.onStartStop();
        }
        assertEquals(stateMachineUITestThing.timeValue, 99);
    }

    //tests that after about 3 seconds, state machine moves to running state
    @Test
    public void testTickEvent() throws Exception {
        stateMachine.onStartStop();
        RunningState runningState = new RunningState(stateMachine);
        Thread.sleep(3050);
        assertEquals(runningState.getId(), stateMachine.getState().getId());
    }

    //tests that about one second after the running state begins, the display decrements by 1
    @Test
    public void testDecrement() throws Exception {
        stateMachine.onStartStop();
        assertEquals(stateMachineUITestThing.timeValue, 1);
        Thread.sleep(4050);
        assertEquals(stateMachineUITestThing.timeValue, 0);
    }

    /*tests that while in running state, pressing cancel
    button returns to stopped state and changes value to 0*/
    @Test
    public void testCancel() throws Exception{
        StoppedState stoppedState = new StoppedState(stateMachine);
        for (int i = 0; i < 5; i ++) {
            stateMachine.onStartStop();
        }
        Thread.sleep(4050);
        assertEquals(stateMachineUITestThing.timeValue, 4);
        stateMachine.onStartStop();
        assertEquals(stateMachineUITestThing.timeValue, 0);
        assertEquals(stoppedState.getId(), stateMachine.getState().getId());
    }

    //tests that alarm will continue alarming until the button is pressed
    @Test
    public void testAlarm() throws Exception {
        AlarmState alarmState = new AlarmState(stateMachine);
        StoppedState stoppedState = new StoppedState(stateMachine);
        stateMachine.onStartStop();
        assertEquals(stateMachineUITestThing.timeValue, 1);
        Thread.sleep(5050);
        assertEquals(stateMachineUITestThing.timeValue, 0);
        assertEquals(alarmState.getId(), stateMachine.getState().getId());
        assertEquals(true, stateMachineUITestThing.alarming);
        stateMachine.onStartStop();
        assertEquals(stoppedState.getId(), stateMachine.getState().getId());
        assertEquals(false, stateMachineUITestThing.alarming);
    }
}
