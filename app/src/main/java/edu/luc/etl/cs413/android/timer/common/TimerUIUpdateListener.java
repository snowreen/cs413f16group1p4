package edu.luc.etl.cs413.android.timer.common;

/**
 * A listener for UI update notifications.
 */

public interface TimerUIUpdateListener {
    void updateState(int stateId);
    void updateTime(int timeValue);
    void playDefaultAlarm();
    void stopDefaultAlarm();
    void playBeep();
}

