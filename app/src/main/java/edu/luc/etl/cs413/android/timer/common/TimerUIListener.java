package edu.luc.etl.cs413.android.timer.common;

/**
 * A listener for timer events coming from the UI.
 */

public interface TimerUIListener {
    void onStartStop();
    void onDisplay(Integer InputCount);
}
