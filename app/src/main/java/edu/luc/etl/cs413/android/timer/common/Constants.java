package edu.luc.etl.cs413.android.timer.common;

/**
 * Constants for the time calculations used by the timer.
 */

public final class Constants {

    public static int SEC_PER_TICK = 1;
    public static int TIME_INC_PER_CLICK = 1;
    public static int MAX_IDLE_TIME = 3;
    public static int MAX_COUNT = 99;
    public static int MIN_COUNT = 0;

    private Constants() { }
}