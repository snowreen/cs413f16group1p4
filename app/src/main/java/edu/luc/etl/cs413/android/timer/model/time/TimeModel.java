package edu.luc.etl.cs413.android.timer.model.time;

public interface TimeModel {
    void resetRuntime();
    void incRuntime();
    void decRuntime();
    int getRuntime();
    boolean isFull();
    boolean isEmpty();
    void resetIdleTime();
    void incIdleTime();
    int getIdleTime();
    boolean isIdleTimeMax();
    void getInput(int input);
}
