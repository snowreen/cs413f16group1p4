package edu.luc.etl.cs413.android.timer.model;

import edu.luc.etl.cs413.android.timer.common.TimerUIUpdateListener;
import edu.luc.etl.cs413.android.timer.model.clock.ClockModel;
import edu.luc.etl.cs413.android.timer.model.clock.DefaultClockModel;
import edu.luc.etl.cs413.android.timer.model.state.DefaultTimerStateMachine;
import edu.luc.etl.cs413.android.timer.model.state.TimerStateMachine;
import edu.luc.etl.cs413.android.timer.model.time.DefaultTimeModel;
import edu.luc.etl.cs413.android.timer.model.time.TimeModel;

/**
 * An implementation of the model facade.
 **/

public class ConcreteTimerModelFacade implements TimerModelFacade {

    private TimerStateMachine stateMachine;
    private ClockModel clockModel;
    private TimeModel timeModel;

    public ConcreteTimerModelFacade() {
        timeModel = new DefaultTimeModel();
        clockModel = new DefaultClockModel();
        stateMachine = new DefaultTimerStateMachine(timeModel, clockModel);
        clockModel.setOnTickListener(stateMachine);
    }

    @Override
    public void onStart() {
        stateMachine.actionInit();
    }

    @Override
    public void setUIUpdateListener(final TimerUIUpdateListener listener) {
        stateMachine.setUIUpdateListener(listener);
    }

    @Override
    public void onStartStop() {
        stateMachine.onStartStop();
    }

    @Override
    public void onDisplay(Integer Input) { stateMachine.onDisplay(Input); }

}
